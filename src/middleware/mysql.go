package middleware

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" //Gorm 支持

	iris "gopkg.in/kataras/iris.v6"
)

const (
	//CONTEXT *
	CONTEXT string = "DB"
)

type mySQLMiddleware struct {
	db *gorm.DB
}

func (m *mySQLMiddleware) Serve(ctx *iris.Context) {
	ctx.Set(CONTEXT, m.db)
	ctx.Next()
}

//NewMySQLMiddleware *
func NewMySQLMiddleware(db *gorm.DB) iris.HandlerFunc {

	mmw := &mySQLMiddleware{db: db}

	return mmw.Serve
}
